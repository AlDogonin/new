﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using ForJunior.Models;

namespace ForJunior.Controllers
{
    public class Context : DbContext
    {
        public Context()
            : base("film")
        {
            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<Context>());
        }
        public DbSet<Film> Film { get; set; }
    }

    public class NewFilmController : Controller
    {
        public RedirectResult ActionForm(RegistrationNewFilm newFilm)
        {
            Film film = new Film {Name = newFilm.FilmName, Autor = HttpContext.User.Identity.Name, Year = newFilm.Year, Poster = newFilm.Poster, Descriotion = newFilm.Description,
             Produser = newFilm.Producer };
            Context contex = new Context();
            contex.Film.Add(film);
            if (contex.SaveChanges() > 0)
            {
                return Redirect("../Home/Succes");
            }
            return Redirect("../Home/Error");
        }

    }
}
