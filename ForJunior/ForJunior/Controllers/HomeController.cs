﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ForJunior.Models;

namespace ForJunior.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Message = "Тестовое задание: каталог фильмов, реализована возиожность просмотра списка фильмов в каталоге, " +
            "добавления фильмов в католог (для вошедших на сайт пользователей).";

            return View();
        }
        public ActionResult RegistrationNewFilm()
        {
            ViewBag.Message = "Страница регистрации нового фильма.";

            return View();
        }
        public ActionResult Succes()
        {
            ViewBag.Message = "Фильм успешно дабавлен в каталог!";

            return View();
        }
        public ActionResult Error()
        {
            ViewBag.Message = "Фильм в каталог не добавлен!";

            return View();
        }
        public ActionResult LookFilm(int id)
        {
            //ViewBag.Message = "";
            Context contex = new Context();
            var listFilmDB = contex.Film;

            var listFilm = listFilmDB.Where(film => film.Id == id);
            listFilm.GetEnumerator().MoveNext();
            Film film1 = listFilm.GetEnumerator().Current;
            Film currentFilm = new Film();


            foreach (Film film in listFilm)
            {
                currentFilm = film;
            }

            return View(currentFilm);
        }
        public ActionResult LooklistFilms()
        {
            Context contex = new Context();
            var listFilmDB = contex.Film;
            ListFilms listFilm = new ListFilms();

            foreach (Film a in listFilmDB)
            {
                listFilm.listFilms.Add(new RegistrationNewFilm { FilmName = a.Name, Description = a.Descriotion, Poster = a.Poster, Producer = a.Produser, Year = a.Year, ID = a.Id});
            }

            return View(listFilm);
        }
    }
}
